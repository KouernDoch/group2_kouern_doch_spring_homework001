package com.testrest.restapi2.controller;

import com.testrest.restapi2.Customer;
import com.testrest.restapi2.CustomerResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/customers")
public class CustomerController {
    ArrayList<Customer> customerlist=new ArrayList<>();
    int id=0;

    @GetMapping("/{id}")
    public ResponseEntity<?> getCustomerById(@PathVariable("id") int Id ){
        for (Customer customer:customerlist) {
            if(customer.getId() == Id){
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "Found customer",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
    @PutMapping("/{CustomerId}")
    public ResponseEntity<?> Update(@RequestParam("id") int Id, @RequestBody Customer customerUpdate){
        for (Customer customer:customerlist) {
            if(customer.getId()==Id){
                customer.setName(customerUpdate.getName());
                customer.setGender(customerUpdate.getGender());
                customer.setAddress(customerUpdate.getAddress());
                customer.setAge(customerUpdate.getAge());
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "Update Successfully",
                        customerUpdate,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
        return ResponseEntity.notFound().build();
    }
    @DeleteMapping("/{CustomerId}")
    public ResponseEntity<?> deleteCustomer(@RequestParam int id){
        for (Customer customer:customerlist) {
            if(customer.getId()==id){
            customerlist.remove(customer);
            return ResponseEntity.ok(new CustomerResponse<>(
                    "Deleted success",
                    customer,
                    "OK",
                    LocalDateTime.now()
            ));
            }
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("")
    public ResponseEntity<?> getAllCustomer(){
        if(customerlist.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                "Successfully",
                customerlist,
                "OK",
                LocalDateTime.now()
        ));
    }

    @PostMapping("")
    public ResponseEntity<?> createUser( @RequestBody Customer customer){
        id++;
        customer.setId(id);
        customerlist.add(customer);
    return ResponseEntity.ok(new CustomerResponse<Customer>(
            "Successfully",
            customer,
            "OK",
            LocalDateTime.now()
        ));
    }


    @GetMapping("/Search")
    public ResponseEntity<?> getUserByname(@RequestParam String name){
        for (Customer customer:customerlist) {
            if(customer.getName().equals(name)){
                return ResponseEntity.ok(new CustomerResponse<Customer>(
                        "Customer found!",
                        customer,
                        "OK",
                        LocalDateTime.now()
                ));
            }
        }
            return ResponseEntity.notFound().build();
    }


}
