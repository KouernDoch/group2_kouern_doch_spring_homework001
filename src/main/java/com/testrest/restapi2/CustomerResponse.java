package com.testrest.restapi2;

import javax.xml.crypto.Data;
import java.time.LocalDateTime;
import java.util.Date;
import java.time.LocalDate;
import java.util.PrimitiveIterator;

public class CustomerResponse<T> {
    private String message;
    private T Customer;
    private String status;
    private LocalDateTime dateTime;

    public CustomerResponse(String message, T Customer ,String status, LocalDateTime dateTime) {
        this.message = message;
        this.Customer = Customer;
        this.status = status;
        this.dateTime = dateTime;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getCustomer() {
        return Customer;
    }

    public void setCustomer(T customer) {
        Customer = customer;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
}
